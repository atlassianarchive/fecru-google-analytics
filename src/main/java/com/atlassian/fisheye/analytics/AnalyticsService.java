package com.atlassian.fisheye.analytics;


import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 */
public class AnalyticsService extends HttpServlet {

    private final TemplateRenderer templateRenderer;
    private final ConfigurationManager configurationManager;


    @Autowired
    public AnalyticsService(TemplateRenderer templateRenderer, ConfigurationManager configurationManager) {
        this.templateRenderer = templateRenderer;
        this.configurationManager = configurationManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final HashMap<String, Object> context = new HashMap<String, Object>();
        final String trackingID = configurationManager.getTrackingID();
        if (trackingID != null) {
            context.put("analyticsKey", trackingID);
            templateRenderer.render("/templates/analytics-js.vm", context, resp.getWriter());
        }

    }
}
