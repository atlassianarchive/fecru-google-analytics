package com.atlassian.fisheye.analytics.admin;

import com.atlassian.fisheye.analytics.ConfigurationManager;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 */
public class AdminServlet extends HttpServlet {

    private final TemplateRenderer templateRenderer;
    private final ConfigurationManager configurationManager;
    private final UserManager userManager;

    final static String PARAM_GA_ID = "ga-id";

    @Autowired
    public AdminServlet(TemplateRenderer templateRenderer, ConfigurationManager configurationManager, UserManager userManager) {
        this.templateRenderer = templateRenderer;
        this.configurationManager = configurationManager;
        this.userManager = userManager;
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String username = userManager.getRemoteUsername(req);
        if (username == null || !userManager.isAdmin(username)) {
            resp.setStatus(401);
            return;
        }

        final String key = req.getParameter(PARAM_GA_ID);
        final HashMap<String, Object> context = new HashMap<String, Object>();

        final String storedKey;
        if (key != null) {
            configurationManager.setTrackingID(key);
            storedKey = key;
        } else {
            storedKey = configurationManager.getTrackingID();
        }

        context.put(PARAM_GA_ID, storedKey);
        resp.setContentType("text/html;charset=utf-8");
        templateRenderer.render("/templates/admin.vm", context, resp.getWriter());

    }
}
