package com.atlassian.fisheye.analytics;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

public class ConfigurationManager {

    // now and attach the auth-token to the global space
    private static final String PLUGIN_STORAGE_KEY = null;
    private static final String GA_TRACKING_ID = "ga-tracking-id";


    private final PluginSettingsFactory pluginSettingsFactory;

    public ConfigurationManager(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    public String getTrackingID() {
        return getValue(GA_TRACKING_ID);
    }

    public void setTrackingID(String trackingID) {
        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY);
        settings.put(GA_TRACKING_ID, trackingID);
    }

    private String getValue(String storageKey) {
        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY);
        Object storedValue = settings.get(storageKey);
        return storedValue == null ? "" : storedValue.toString();
    }


}